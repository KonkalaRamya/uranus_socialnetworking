<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<style type="text/css">
body{
	background-color:pink;
	padding:0;
	margin:0;
	font: 72.5% 'Helvetica Neue', Helvetica, Arial, sans-serif; 
}
.wrapper{
	width:820px;
	margin:30px auto;
	padding:25px;
	min-height:400px;
	height:auto;	
}
.wrapper h2 { margin-top:30px;}

.box h3{
	text-align:center;
	position:relative;
	top:80px;
}
.box {
	width:70%;
	height:200px;
	background:#FFF;
	margin:40px auto;
}

/*==================================================
 * Effect 1
 * ===============================================*/
 .effect1{
	-webkit-box-shadow: 0 10px 6px -11px #777;
	   -moz-box-shadow: 0 10px 6px -11px #777;
	        box-shadow: 0 10px 6px -11px #777;
}

</style>
<title>Insert title here</title>
</head>
<body>
<section class="wrapper">
	<div class="box effect1">
		<h3>Effect 1</h3>
	</div>
	
</section>
</body>
</html>